import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");


describe('Bonus system tests', () => {
    let app;
    console.log("Tests started");


    test('Standard < 10000 test',  (done) => {
        let a = "Standard";
        let b = 9999;
        expect(calculateBonuses(a,b)).toEqual(0.05);
        done();
    });

    test('Standard < 50000 > 10000 test',  (done) => {
        let a = "Standard";
        let b = 10001;
        expect(calculateBonuses(a,b)).toEqual(0.05 * 1.5);
        done();
    });

    test('Standard 10000 test',  (done) => {
        let a = "Standard";
        let b = 10000;
        expect(calculateBonuses(a,b)).toEqual(0.05 * 1.5);
        done();
    });

    test('Standard < 100000 > 50000 test',  (done) => {
        let a = "Standard";
        let b = 50001;
        expect(calculateBonuses(a,b)).toEqual(0.05 * 2);
        done();
    });

    test('Standard 50000 test',  (done) => {
        let a = "Standard";
        let b = 50000;
        expect(calculateBonuses(a,b)).toEqual(0.05 * 2);
        done();
    });

    test('Standard > 100000 test',  (done) => {
        let a = "Standard";
        let b = 100001;
        expect(calculateBonuses(a,b)).toEqual(0.05 * 2.5);
        done();
    });

    test('Standard 100000 test',  (done) => {
        let a = "Standard";
        let b = 100000;
        expect(calculateBonuses(a,b)).toEqual(0.05 * 2.5);
        done();
    });

    test('Premium < 10000 test',  (done) => {
        let a = "Premium";
        let b = 1;
        expect(calculateBonuses(a,b)).toEqual(0.1);
        done();
    });

    test('Premium < 50000 > 10000 test',  (done) => {
        let a = "Premium";
        let b = 10001;
        expect(calculateBonuses(a,b)).toEqual(0.1 * 1.5);
        done();
    });

    test('Premium < 100000 > 50000 test',  (done) => {
        let a = "Premium";
        let b = 50001;
        expect(calculateBonuses(a,b)).toEqual(0.1 * 2);
        done();
    });

    test('Premium > 100000 test',  (done) => {
        let a = "Premium";
        let b = 100001;
        expect(calculateBonuses(a,b)).toEqual(0.1 * 2.5);
        done();
    });

    test('Premium 100000 test',  (done) => {
        let a = "Premium";
        let b = 100000;
        expect(calculateBonuses(a,b)).toEqual(0.1 * 2.5);
        done();
    });

    test('Premium 50000 test',  (done) => {
        let a = "Premium";
        let b = 50000;
        expect(calculateBonuses(a,b)).toEqual(0.1 * 2);
        done();
    });

    test('Diamond < 10000 test',  (done) => {
        let a = "Diamond";
        let b = 1;
        expect(calculateBonuses(a,b)).toEqual(0.2);
        done();
    });

    test('Diamond < 50000 > 10000 test',  (done) => {
        let a = "Diamond";
        let b = 10001;
        expect(calculateBonuses(a,b)).toEqual(0.2 * 1.5);
        done();
    });

    test('Diamond < 100000 > 50000 test',  (done) => {
        let a = "Diamond";
        let b = 50001;
        expect(calculateBonuses(a,b)).toEqual(0.2 * 2);
        done();
    });

    test('Diamond > 100000 test',  (done) => {
        let a = "Diamond";
        let b = 100001;
        expect(calculateBonuses(a,b)).toEqual(0.2 * 2.5);
        done();
    });

    test('Diamond 100000 test',  (done) => {
        let a = "Diamond";
        let b = 100000;
        expect(calculateBonuses(a,b)).toEqual(0.2 * 2.5);
        done();
    });

    test('Diamond 50000 test',  (done) => {
        let a = "Diamond";
        let b = 50000;
        expect(calculateBonuses(a,b)).toEqual(0.2 * 2);
        done();
    });

    test('nonsense 1 test',  (done) => {
        let a = "nonsense";
        let b = 1;
        expect(calculateBonuses(a,b)).toEqual(0);
        done();
    });

    test('nonsense 10000 test',  (done) => {
        let a = "nonsense";
        let b = 10000;
        expect(calculateBonuses(a,b)).toEqual(0);
        done();
    });

    test('nonsense 50000 test',  (done) => {
        let a = "nonsense";
        let b = 50000;
        expect(calculateBonuses(a,b)).toEqual(0);
        done();
    });

    test('nonsense 100000 test',  (done) => {
        let a = "nonsense";
        let b = 100000;
        expect(calculateBonuses(a,b)).toEqual(0);
        done();
    });

    console.log('Tests Finished');

});
